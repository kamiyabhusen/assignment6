/**
 * AccountController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * @route POST /api/account/
     * @access private
     * @description :: Create account
     */
    create:async function(req,res) {
        try {
            
            //validate the req body
            const schema = ValidationSchemaService.createAccount();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            //creating account in database
            const account = await Account.create(req.body).fetch();


            res.ok({account});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/accounts/
     * @access private 
     * @description :: Get ALL account of current user
     */
    find:async function(req,res) {
        try {

            //For Pagination
            let perPage = 5,page = 1;

            //if perpage not given in query assign default
            if(req.query.perpage){
                perPage = req.query.perpage;
            }

            //if page not given in query assign default
            if(req.query.page){
                page = req.query.page;
            }

            //count total number of accounts
            const totalAccount = await Account.count({
                user:req.user
            });
            
            
            //get all accounts based on page number and perpage 
            const accounts = await Account.find({
                where:{user:req.user},
                skip: perPage * (page -1),
                limit: perPage
            });
            
            res.ok({
                accounts,
                total:totalAccount,
                page,
                perPage
            });

        } catch (error) {

            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/accounts/:id
     * @access private 
     * @description :: GET specific account
     */
    findOne:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');
            //get package by id
            const account = await Account.findOne({id});

            //if there is no account
            if(!account){
                return res.status(404).json({error:"No Account"});
            }
            
            res.ok({account});

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route PUT /api/accounts/:id
     * @access private
     * @description :: Update specific account
     */
    update:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //validate the req body
            const schema = ValidationSchemaService.updateAccount();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }
            
            //update the records by id
            const account = await Account.updateOne({id}).set(req.body);

            res.ok({account});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route DELETE /api/account/:id
     * @access private
     * @description :: delete specific account
     */
    delete:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //delte the records by id
            const account = await Account.destroyOne({id});

            res.ok({account});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },
};

