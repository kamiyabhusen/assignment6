/**
 * TranscationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    /**
     * @route POST /api/transcations/
     * @access private
     * @description :: Create transcation
     */
    create:async function(req,res) {
        try {
            
            //validate the req body
            const schema = ValidationSchemaService.createTranscation();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            //creating transcation in database
            const transcation = await Transcation.create(req.body).fetch();


            res.ok({transcation});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/transcations/:id
     * @access private 
     * @description :: Get ALL transcations of given account
     */
    find:async function(req,res) {
        try {

            //get id form params
            const accountid = req.param('id');

            //For Pagination
            let perPage = 5,page = 1;

            //if perpage not given in query assign default
            if(req.query.perpage){
                perPage = req.query.perpage;
            }

            //if page not given in query assign default
            if(req.query.page){
                page = req.query.page;
            }

            //count total number of transcation
            const totalTranscation = await Transcation.count({
                accountHolder:accountid
            });
            
            
            //get all transcations based on page number and perpage and sorting in DESC
            const transcations = await Transcation.find({
                where:{accountHolder:accountid},
            }).sort("createdAt DESC").skip(perPage * (page -1)).limit(perPage);
            
            
            res.ok({
                transcations,
                total:totalTranscation,
                page,
                perPage
            });

        } catch (error) {

            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },


    /**
     * @route GET /api/transcations/one/:id
     * @access private 
     * @description :: GET specific transcation
     */
    findOne:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //get transcation by id
            const transcation = await Transcation.findOne({id}).populate("accountHolder");

            //if there is no transcation
            if(!transcation){
                return res.status(404).json({error:"No Transcation"});
            }
            
            res.ok({transcation});

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },


    /**
     * @route PUT /api/transcation/:id
     * @access private
     * @description :: Update specific transcation
     */
    update:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //validate the req body
            const schema = ValidationSchemaService.updateTranscation();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }
            
            //update the records by id
            const transcation = await Transcation.updateOne({id}).set(req.body);

            res.ok({transcation});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route DELETE /api/transcations/:id
     * @access private
     * @description :: delete specific transcation
     */
    delete:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //delte the records by id
            const transcation = await Transcation.destroyOne({id});

            res.ok({transcation});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },
};

