/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
  
    /**
     * @route POST /api/users
     * @access public
     * @description :: to Create user(register)
     */
    create:async function(req,res) {

        try {
            //validation the req.body
            const schema = ValidationSchemaService.createUser()
            const { value, error:validationError } = schema.validate(req.body);
            
            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            //check email is already exist
            const isEmailExist = await User.findOne({email:req.body.email});
            if(isEmailExist){
                return res.badRequest({error:"email is already exist."});
            }

            //encrypt the passowrd
            const encryptedPassword = await PasswordService.hashPassword(req.body.password);


            //create user in database
            const createdUser = await User.create({
                ...req.body,
                password:encryptedPassword
            }).fetch();

            //sending the welcome email
            const info = await UtilsService.welcomeMail(createdUser.email);

            //creating default account
            const defaultAccount = await Account.create({
                name:"default",
                email:createdUser.email,
                user:createdUser.id
            })

            //generating token
            const token = JWTService.createJWT({
                id:createdUser.id
            })

            //returning token and username, typeofuser
            res.ok({
                token,
                id:createdUser.id,
                name:createdUser.name,
            });
            
        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }

    },

     /**
     * @route POST /api/users/login
     * @access public
     * @description :: User Login function
     */
    login:async function(req,res) {
        try {
            //validation the req.body
            const schema = ValidationSchemaService.login()
            const { value, error:validationError } = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            //check username is correct or not
            const user = await User.findOne({email:req.body.email});
            if(!user){
                return res.badRequest({error:"email is not register!"});
            }

            //comparing passwords
            const matchedPassword = await PasswordService.comparePassword(req.body.password,user.password);
            if(!matchedPassword){
                return res.badRequest({error:"Password is invalid"});
            }

            //generating token
            const token = JWTService.createJWT({
                id:user.id
            })

            //returning token and username
            res.ok({
                token,
                name:user.name,
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/users/me
     * @access private 
     * @description :: get current user details
     */
    me:async function(req,res) {
        try {
            
            //get current user
            const user = await User.findOne({id:req.user});
            
            res.ok({
                name:user.name,
                id:user.id
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },
};

