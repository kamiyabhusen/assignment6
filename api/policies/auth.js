/**
 * 
 * @description this policy check user is logged in or not
 */


module.exports = async function(req,res,next) {
    //check token is in header or not    
    if(!req.headers.authorization){
        return res.status(401).json({error:"Token is not Provided"});
    }
    const token = req.headers.authorization;

    //verify the token with JWTSERVICE
    let decorded;
    try{
        decorded = JWTService.verify(token);
    }catch(err){
        console.log(err.message);
        return res.status(401).json({error:"Token is Invalid"});
    }

    req.user = decorded.id;
    next();
}