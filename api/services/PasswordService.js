/**
 * Password Service
 *
 * @description :: this service for hashing password and comparing the password
 */
const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports = {

    //for hashing password
    hashPassword: async function(password) {
        
        //return promise with hashed password
        return await bcrypt.hash(password,saltRounds);

    },

    //for comparing password
    comparePassword:async function(password,hash) {
        
        //return promise 
        return await bcrypt.compare(password,hash);

    }

    
}