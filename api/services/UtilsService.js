/**
 * Util Service
 *
 * @description :: utils services.
 */

const nodemailer = require('nodemailer');

module.exports = {
    welcomeMail: async function(email) {
            const transporter = nodemailer.createTransport({
                host:'smtp.mailtrap.io',
                port:2525,
                auth: {
                       user: process.env.NM_USER,
                       pass: process.env.NM_PASSWORD
                   }
               });
    
               let info = await transporter.sendMail({
                    from:'lyeadam55@gmail.com',
                    to:email,
                    subject:"Welcome to Expensify",
                    text:"Thank You for registering. we hope You will increase your savings with Expensify"
               });
               return info;
    }
}