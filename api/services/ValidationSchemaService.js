/**
 * validation schema Service
 *
 * @description :: this service for defining the validation schemas
 */

const Joi = require('joi');
module.exports = {
    //create user schema
    createUser: function() {
      
        const schema = Joi.object({
            name:Joi.string()
                .min(3)
                .max(30)
                .required(),
            email:Joi.string()
                .email()
                .required(),
            password:Joi.string()
                .min(8)
                .max(30)
                .required()
        });
        return schema;
        
    },
    // //create user schema
    // updateUser: function() {
      
    //     const schema = Joi.object({
    //         username:Joi.string()
    //             .alphanum()
    //             .min(3)
    //             .max(30)
    //             .required(),
    //         email:Joi.string()
    //             .email()
    //             .required(),
    //         typeOfUser:Joi.string()
    //             .required()
    //             .pattern(/(main|business)/),
    //         package:Joi.string(),
    //         status:Joi.string()
    //             .pattern(/(active|inactive)/)
    //     });
    //     return schema;
        
    // },

    //login input schema
    login: function() {
      
        const schema = Joi.object({
            email:Joi.string()
                .email()
                .required(),
            password:Joi.string()
                .min(8)
                .max(30)
                .required()
        });
        return schema;
        
    },

    //create package schema
    createAccount:function() {
        const schema = Joi.object({
            name:Joi.string()
                .required(),
            email:Joi.string()
                .email()
                .required(),
            user:Joi.string()
                .required()
        });
        return schema;
    },

    //create package schema
    updateAccount:function() {
        const schema = Joi.object({
            name:Joi.string()
                .required(),
            email:Joi.string()
                .email()
                .required()
        });
        return schema;
    },

    //create package schema
    createTranscation:function() {
        const schema = Joi.object({
            typeOfTranscation:Joi.string()
                .required()
                .pattern(/(income|expense)/),
            transfer:Joi.number()
                .required(),
            category:Joi.string()
                .required(),
            accountHolder:Joi.string()
                .required()
        });
        return schema;
    },

    //create package schema
    updateTranscation:function() {
        const schema = Joi.object({
            typeOfTranscation:Joi.string()
                .required()
                .pattern(/(income|expense)/),
            transfer:Joi.number()
                .required(),
            category:Joi.string()
                .required()
        });
        return schema;
    }
}