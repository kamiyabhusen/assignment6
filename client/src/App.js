import React, { useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

//layout
import UserLayout from './components/UserLayout';


//components
import AlertBox from './components/AlertBox';

//Redux
import store from './store';
import setAuthToken from './utils/setAuthToken';
import { loadUser } from './redux/actions/userActions'

//Pages
import RegisterPage from './pages/Register';
import LoginPage from './pages/Login';

//css
import './App.css';
import './assets/argon-dashboard-react.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";

function App() {

  useEffect(() => {
    
    // check for token in LS
    if (localStorage.token) {
      //setAuth token in axios header
      setAuthToken(localStorage.getItem('token'));
    }

    //load the user from backend
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <AlertBox />
        <Switch>
          <Route path="/register" exact component={RegisterPage} />
          <Route path="/login" exact component={LoginPage} />
          <Route path="/" render={(props) => <UserLayout {...props} />} /> 
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
