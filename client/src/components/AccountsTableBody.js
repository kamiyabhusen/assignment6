import React from "react";
import {Link, NavLink as NavLinkRRD} from 'react-router-dom'

//reactstrap imports
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

export default function AccountsTableBody(props) {


    //handles delete button will call parent component handledelete with id
    const handleDeleteBtn = () => {
        
        props.handleDelete(props.account.id);
    }
  return (
    <tr>
      <td>
        <span className="mb-0 text-sm">{props.index}</span>
      </td>
      <td>{props.account.name}</td>
      <td>{props.account.email}</td>
      <td>
        <Link to={"/transcations/"+props.account.id}>
          View Transcation
        </Link>
      </td>
      <td className="text-right">
        <UncontrolledDropdown>
          <DropdownToggle
            className="btn-icon-only text-light"
            href="#pablo"
            role="button"
            size="sm"
            color=""
            onClick={(e) => e.preventDefault()}
          >
            <i className="fas fa-ellipsis-v" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-arrow">
            <DropdownItem to={"/accounts/edit/"+props.account.id} tag={NavLinkRRD}>
              Edit
            </DropdownItem>
            <DropdownItem onClick={handleDeleteBtn}>
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
}
