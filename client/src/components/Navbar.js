import React from 'react'
import { Link, NavLink as NavLinkRRD } from 'react-router-dom'
import { 
    Col, 
    Container,  
    Nav, 
    Navbar, 
    NavbarBrand, 
    NavItem, 
    NavLink, 
    Row, 
    UncontrolledCollapse, 
} from 'reactstrap'
import { useDispatch,useSelector } from 'react-redux';
import { logout  } from '../redux/actions/userActions';

export default function UserNavbar() {

    const { user } = useSelector(state => state.user);
    const dispatch = useDispatch();

    //logout button handler
    const handleLogout = () => {
      dispatch(logout());
    }

    return (
        <Navbar
          className="navbar-horizontal navbar-dark bg-primary"
          expand="lg"
        >
          <Container>
            <NavbarBrand to="/" tag={NavLinkRRD}>
              Expancify
            </NavbarBrand>
            <button
              aria-controls="navbar-primary"
              aria-expanded={false}
              aria-label="Toggle navigation"
              className="navbar-toggler"
              data-target="#navbar-primary"
              data-toggle="collapse"
              id="navbar-primary"
              type="button"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-primary">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <Link to="/">
                      Expencify
                    </Link>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button
                      aria-controls="navbar-primary"
                      aria-expanded={false}
                      aria-label="Toggle navigation"
                      className="navbar-toggler"
                      data-target="#navbar-primary"
                      data-toggle="collapse"
                      id="navbar-primary"
                      type="button"
                    >
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-lg-auto" navbar>
                <NavItem>
                  <NavLink to="/" tag={NavLinkRRD}>
                    {user ? user.name : ""}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink onClick={handleLogout}>
                    Logout
                  </NavLink>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
    )
}
