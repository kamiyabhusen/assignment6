import React from "react";
import { NavLink as NavLinkRRD} from 'react-router-dom'

//reactstrap imports
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

export default function TranscationTableBody(props) {


    const handleDeleteBtn = () => {
        
        props.handleDelete(props.transcation.id);
    }
  return (
    <tr>
      <td>
        <span className="mb-0 text-sm">{props.index}</span>
      </td>
      <td>{props.transcation.typeOfTranscation}</td>
      <td>{props.transcation.transfer}</td>
      <td>{props.transcation.category}</td>
      <td className="text-right">
        <UncontrolledDropdown>
          <DropdownToggle
            className="btn-icon-only text-light"
            href="#pablo"
            role="button"
            size="sm"
            color=""
            onClick={(e) => e.preventDefault()}
          >
            <i className="fas fa-ellipsis-v" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-arrow">
            <DropdownItem to={"/transcations/edit/"+props.transcation.id} tag={NavLinkRRD}>
              Edit
            </DropdownItem>
            <DropdownItem onClick={handleDeleteBtn}>
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
}
