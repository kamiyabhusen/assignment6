import React from 'react'
import { Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import UserNavbar from './Navbar';
import PrivateRoute from './routing/PrivateRoute';

//pages
import HomePage from '../pages/Home';
import AddAccount from '../pages/AddAcount';
import EditAccount from '../pages/EditAccount';
import TranscationPage from '../pages/Transcations';
import AddTranscationPage from '../pages/AddTranscation';
import EditTranscationPage from '../pages/EditTranscation';

export default function UserLayout() {
    return (
        <>
            <UserNavbar />
            <Container>
                <Switch>
                    <PrivateRoute path="/transcations/edit/:id" exact component={EditTranscationPage} />
                    <PrivateRoute path="/transcations/add/:accountid" exact component={AddTranscationPage} />
                    <PrivateRoute path="/transcations/:accountid" exact component={TranscationPage} />
                    <PrivateRoute path="/accounts/edit/:id" exact component={EditAccount} />
                    <PrivateRoute path="/accounts/add" exact component={AddAccount} />
                    <PrivateRoute path="/" exact component={HomePage} />
                </Switch>
            </Container>
        </>
    )
}
