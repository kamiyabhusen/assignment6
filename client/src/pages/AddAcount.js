import React, { useState } from 'react'
import { useDispatch,useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";

import { alert } from '../redux/actions/alertActions';
import api from '../utils/api';

export default function AddAccount() {
    
    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [redirect,setRedirect] = useState(false);

    const { user } = useSelector(state => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        try {
            
          await api.post("/accounts",{
              name,
              email,
              user:user.id
          });
          dispatch(alert("user Created successfully",'success'));
          setRedirect(true); //for redirection of page
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    //redirect page after 200 response
    if(redirect){
        return <Redirect to="/" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add Account</h3>
            </CardHeader>
            <CardBody>
              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-username">
                    Name
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Name"
                    type="text"
                    name="name"
                    required
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Email
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Email"
                    type="email"
                    name="email"
                    required
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormGroup>
                <Button color="primary" type="submit">
                  Add
                </Button>
                <Button color="danger" onClick={() => setRedirect(true)} type="button">
                  cancel
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

