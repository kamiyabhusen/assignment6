import React, { useEffect, useState } from 'react'
import { useDispatch,useSelector } from 'react-redux';
import { Redirect,useParams } from 'react-router-dom';

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";

  import { alert } from '../redux/actions/alertActions';
import api from '../utils/api';

export default function AddTranscation() {

    const { accountid } = useParams();
  
    //to set accounts Details
    const [account, setAccount] = useState({});
    //form fields
    const [typeOfTranscation,setTypeOfTranscation] = useState("income");
    const [transfer,setTransfer] = useState(0);
    const [category,setCategory] = useState("");
    //for redirect back to the page
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();

    //get Details of Account by id
    useEffect(() => {
        (async () => {
            try{
                const accountRes = await api.get("/accounts/"+accountid);
                setAccount(accountRes.data.account);
            }catch(error){
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()
    },[]);


    const handleFormSubmit = async (e) => {
        e.preventDefault();
        try {
            await api.post("/transcations",{
              typeOfTranscation,
              transfer,
              category,
              accountHolder:accountid
            });
            dispatch(alert("Transcation Created successfully",'success'));
            setRedirect(true);
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to={"/transcations/"+accountid} />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add Transcation to {account && account.name}'s Account</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      Transcation Type
                    </label>
                    <Input
                      type="select"
                      onChange={(e) => setTypeOfTranscation(e.target.value)}
                      className="form-control-alternative"
                      name="typeOfUser"
                    >
                      <option value="income">Income</option>
                      <option value="expense">Expense</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Transfer
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Transfer"
                    type="number"
                    name="Transfer"
                    required
                    onChange={(e) => setTransfer(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Category
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Category"
                    type="text"
                    name="Category"
                    required
                    onChange={(e) => setCategory(e.target.value)}
                  />
                </FormGroup>
                <Button color="primary" type="submit">
                  Add
                </Button>
                <Button color="danger" onClick={() => setRedirect(true)} type="button">
                  cancel
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

