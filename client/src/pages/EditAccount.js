import React, { useState, useEffect } from 'react'

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { useDispatch } from 'react-redux';
import { alert } from '../redux/actions/alertActions';
import api from '../utils/api';
import { Redirect, useParams  } from 'react-router-dom';

export default function EditUser() {

    const { id } = useParams();

    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();


    useEffect(() => {
        (async () => {
            try {
                const res = await api.get("/accounts/"+id);
                const resData = res.data.account;
                setName(resData.name);
                setEmail(resData.email);
            } catch (error) {
                console.log(error);
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()
    },[]);


    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            
            await api.put("/accounts/"+id,{
                name,
                email,
            });
            dispatch(alert("Account Updated successfully",'success'));
           setRedirect(true);
        } catch (error) {
            console.log(error.response);
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to="/" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Edit Account</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-username">
                    Username
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="name"
                    type="text"
                    name="name"
                    value={name}
                    required
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Email
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Email"
                    type="email"
                    name="email"
                    value={email}
                    required
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormGroup>
                <Button color="primary" type="submit">
                  Update
                </Button>
                <Button color="danger" onClick={() => setRedirect(true)} type="button">
                  cancel
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

