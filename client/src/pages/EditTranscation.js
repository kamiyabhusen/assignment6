import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { Redirect, useParams  } from 'react-router-dom';

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { alert } from '../redux/actions/alertActions';
import api from '../utils/api';

export default function EditTranscation() {

    const { id } = useParams();

    //for seting accounts
    const [account, setAccount] = useState({});
    //form Fields
    const [typeOfTranscation,setTypeOfTranscation] = useState("income");
    const [transfer,setTransfer] = useState(0);
    const [category,setCategory] = useState("");
    const [accountid,setAccountid] = useState("")
    
    //for redirect back to page
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();


    useEffect(() => {
        
        (async () => {
            try {
                const res = await api.get("/transcations/one/"+id);
                const resData = res.data.transcation;
                setTypeOfTranscation(resData.typeOfTranscation);
                setTransfer(resData.transfer);
                setCategory(resData.category);
                setAccountid(resData.accountHolder.id)
                setAccount(resData.accountHolder)
            } catch (error) {
                console.log(error);
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()

    },[]);


    const handleFormSubmit = async (e) => {
        e.preventDefault();
        try {          
            await api.put("/transcations/"+id,{
              typeOfTranscation,
              transfer,
              category
            });
            dispatch(alert("Transcation Updated successfully",'success'));
            setRedirect(true);
        } catch (error) {
            console.log(error.response);
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to={"/transcations/"+accountid} />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Edit Transcation of {account && account.name}'s Account</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
              <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      Transcation Type
                    </label>
                    <Input
                      type="select"
                      onChange={(e) => setTypeOfTranscation(e.target.value)}
                      className="form-control-alternative"
                      name="typeOfUser"
                    >
                      <option value="income" selected={typeOfTranscation === "income"}>Income</option>
                      <option value="expense" selected={typeOfTranscation === "expense"}>Expense</option>
                    </Input>
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Transfer
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Transfer"
                    type="number"
                    value={transfer}
                    name="Transfer"
                    required
                    onChange={(e) => setTransfer(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Category
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Category"
                    type="text"
                    value={category}
                    name="Category"
                    required
                    onChange={(e) => setCategory(e.target.value)}
                  />
                </FormGroup>
                <Button color="primary" type="submit">
                  Update
                </Button>
                <Button color="danger" onClick={() => setRedirect(true)} type="button">
                  cancel
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

