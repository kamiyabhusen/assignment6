import React, { useEffect, useState } from 'react'
import { useSelector,useDispatch } from 'react-redux';
import { 
    Button,
    Card,
    CardFooter,
    CardHeader,
    Col,
    Container,
    Row,
    Table 
} from 'reactstrap';
import { NavLink as NavLinkRRD } from 'react-router-dom'
import CustomPagination from '../components/CustomPagination';
import AccountsTableBody from '../components/AccountsTableBody';
import api from '../utils/api';
import { alert } from '../redux/actions/alertActions';
import { Redirect } from 'react-router-dom';

export default function Home() {
    
    const [accounts,setAccounts] = useState([]);
    const [page,setPage] = useState(1);
    const [perPage,setPerPage] = useState(5);
    const [totalRecords,setTotalRecords] = useState(0);
    
    
    const dispatch = useDispatch();


    useEffect(() => {
        getData();
    },[]);

    useEffect(() => {
        getData();
    },[page]);


    const getData = async () => {
        try {
            const res = await api.get(`/accounts?page=${page}&perpage=${perPage}`);
            const resData = res.data.accounts;
            setAccounts(resData);
            setTotalRecords(res.data.total);
        } catch (error) {
            console.log(error)
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };
    
    const handleDelete =  async (id) => {
        try {            
            const res = await api.delete("/accounts/"+id);
            dispatch(alert("account Deleted successfully",'success'));
            
            //update set when delete the item
            setAccounts(accounts.filter(account => account.id !== id));
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    }

    //set Page when user click on pagination button
    const chnagePage = (page) => {
      setPage(page);
    }
    
    return (
        <Container>
        <Row className="mt-4">
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">All Accounts</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        to="/accounts/add"
                        tag={NavLinkRRD}
                        size="md"
                      >
                        Add New Account
                      </Button>
                    </Col>
                  </Row>
              </CardHeader>
              <Table
                className=" table-flush"
                style={{ minHeight: "200px" }}
                responsive
              >
                <thead className="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">name</th>
                    <th scope="col">email</th>
                    <th scope="col">Transction</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  {accounts.map((account,index) => {
                    let i = index + ( perPage * ( page - 1 ) ) + 1;
                    return <AccountsTableBody handleDelete={handleDelete} index={i} account={account} />;
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <CustomPagination
                  itemPerPage={perPage}
                  page={page}
                  totalRecords={totalRecords}
                  chnagePage={chnagePage}
                />
              </CardFooter>
            </Card>
          </div>
        </Row>
        </Container>
    )
}
