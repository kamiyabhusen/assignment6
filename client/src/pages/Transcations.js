import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { 
    Button,
    Card,
    CardFooter,
    CardHeader,
    Col,
    Container,
    Row,
    Table 
} from 'reactstrap';
import { NavLink as NavLinkRRD } from 'react-router-dom'
import CustomPagination from '../components/CustomPagination';
import TranscationTableBody from '../components/TranscationTableBody';
import api from '../utils/api';
import { alert } from '../redux/actions/alertActions';
import { useParams } from 'react-router-dom';

export default function Home() {
    
    const { accountid } = useParams();

    const [transcations,setTranscations] = useState([]);
    const [account, setAccount] = useState({});
    const [page,setPage] = useState(1);
    const [perPage,setPerPage] = useState(5);
    const [totalRecords,setTotalRecords] = useState(0);
    
    const dispatch = useDispatch();


    useEffect(() => {
        getData();
    },[]);

    useEffect(() => {
        getData();
    },[page]);


    const getData = async () => {
        try {

            const accountRes = await api.get("/accounts/"+accountid);
            setAccount(accountRes.data.account);

            const res = await api.get(`/transcations/${accountid}?page=${page}&perpage=${perPage}`);
            const resData = res.data.transcations;
            setTranscations(resData);
            setTotalRecords(res.data.total);
        } catch (error) {
            console.log(error)
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };
    
    const handleDelete =  async (id) => {
        try { 
            await api.delete("/transcations/"+id);
            dispatch(alert("transcation Deleted successfully",'success'));

            //update set when delete the item
            setTranscations(transcations.filter(transcation => transcation.id !== id));
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    }

    //set Page when user click on pagination button
    const chnagePage = (page) => {
      setPage(page);
    }
    
    return (
        <Container>
        <Row className="mt-4">
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">All Transcations of {account ? account.name :''} Account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        to={"/transcations/add/"+accountid}
                        tag={NavLinkRRD}
                        size="md"
                      >
                        Add New Transcation
                      </Button>
                    </Col>
                  </Row>
              </CardHeader>
              <Table
                className=" table-flush"
                style={{ minHeight: "200px" }}
                responsive
              >
                <thead className="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Type</th>
                    <th scope="col">Transfer</th>
                    <th scope="col">category</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {transcations.map((transcation,index) => {
                    let i = index + ( perPage * ( page - 1 ) ) + 1;
                    return <TranscationTableBody handleDelete={handleDelete} index={i} transcation={transcation} />;
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <CustomPagination
                  itemPerPage={perPage}
                  page={page}
                  totalRecords={totalRecords}
                  chnagePage={chnagePage}
                />
              </CardFooter>
            </Card>
          </div>
        </Row>
        </Container>
    )
}
