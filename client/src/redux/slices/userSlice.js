import { createSlice } from '@reduxjs/toolkit';

//User Slice(state)
const user = createSlice({
    name:"user",
    initialState:{
        token:localStorage.getItem('token'),
        user:null,
        isAuthenticated:false,
        loading:true
    },
    reducers:{
        authStart:(state) => {
            state.loading = true;
        },
        userLoaded:(state,action) => {
            state.user = action.payload;
            state.token = localStorage.getItem('token')
            state.isAuthenticated = true;
            state.loading = false;
        },
        authSuccess:(state,action) => {
            state.user = action.payload;
            state.token = localStorage.getItem('token')
            state.isAuthenticated = true;
            state.loading = false;
        },
        authFail:(state) => {
            state.user = null;
            state.token = null;
            state.isAuthenticated = false;
            state.loading = false;
        }
    }
});

export default user.reducer;

export const { authSuccess, authFail, userLoaded, authStart } = user.actions;