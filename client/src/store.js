import { configureStore,combineReducers } from '@reduxjs/toolkit'

//import reducers
import user from './redux/slices/userSlice';
import alert from './redux/slices/alertSlice';

//All Reducers
const reducer = combineReducers({
    user,
    alert
})

//redux store
const store = configureStore({
    reducer
})

export default store;