/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    //user Routes
    'POST /api/users':'UserController.create',
    'POST /api/users/login':'UserController.login',
    'GET /api/users/me':'UserController.me',

    //Account Routes
    'POST /api/accounts':'AccountController.create',
    'GET /api/accounts':'AccountController.find',
    'GET /api/accounts/:id':'AccountController.findOne',
    'PUT /api/accounts/:id':'AccountController.update',
    'DELETE /api/accounts/:id':'AccountController.delete',
    
    //Transcation Routes
    'POST /api/transcations':'TranscationController.create',
    'GET /api/transcations/:id':'TranscationController.find',
    'GET /api/transcations/one/:id':'TranscationController.findOne',
    'PUT /api/transcations/:id':'TranscationController.update',
    'DELETE /api/transcations/:id':'TranscationController.delete',
};
